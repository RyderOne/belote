# Bot Belote

## Description

This project is a personal work to have an experience with react application and bot development.

This is an implementation of the card game [Belote](https://en.wikipedia.org/wiki/Belote).

## Intention

The intend of this development is to provide some of theses functionalities :

    * Play against BOT with difficulty levels (easiest, easy, moderate, hard)
    * Develop some BOT strategies
    * Implement rules of the classic game
    * Play against another humans and / or BOTs
    * Play online
    * Keep track of played games
    * Statistics ?
    * Spectate a game
    * Replay a game
    
## Thoughts on the technical implementation

    * Games are stored in an SQL database
    * The front game is a REACT full client app
    * Visual effects are optionnals
    * The project is docker based, but you can easily make it work on your computer. For faster development, it uses a private compagny library called zol-common. Forget this target, you don't need it.
    * This is an open source project (despite the ZC part), feel free to participate by doing pull requests or comment
