const express = require('express');
const app = express();

const appPort = process.env.EXPRESS_PORT;

const server = app.listen(appPort, () => {
    console.log(`Example app listening on port ${appPort}!`);
});

const io = require('socket.io').listen(server);

let currentState = new Map();

app.get('/', (req, res) => {
    console.log(Array.from(currentState));
    res.setHeader('Content-Type', 'application/json');
    res.send(Array.from(currentState));
});

io.on('connection', function(socket){
    console.log('a user connected');

    currentState.set(socket.id, {
        is_connected: socket.is_connected,
        date: (new Date()).toISOString()
    });

    socket.on('disconnect', function(){
        console.log('user disconnected');
        currentState.delete(socket.id);
    });

    socket.on('chat message', function(msg){
        console.log('message: ' + msg);
    });
});