.PHONY: no_targets__ list
no_targets__:

# Use it as a debug purpose, to list all available make targets
list:
	sh -c "$(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | sort"

#------------------------------------------------------------------------------------------------
# Project VARS
#------------------------------------------------------------------------------------------------

zol_common_version=v7.3.2
zol_common_entrypoint=.zol/zol-common/makefile/entrypoint.mk

# Trick the library to use npm install commands easily
symfony_dir_name=express

#------------------------------------------------------------------------------------------------
# Include zol common library
#------------------------------------------------------------------------------------------------

ifneq ("$(wildcard $(zol_common_entrypoint))","")
    include $(zol_common_entrypoint)
endif

zol-common:
	@$(shell rm -rf .zol; mkdir -p .zol)
	@cd .zol && git clone --branch $(zol_common_version) git@gitlab.com:zolteam/zol-common.git

#------------------------------------------------------------------------------------------------
# Project related commands
#------------------------------------------------------------------------------------------------

start:
	@echo "$(step) Starting $(project_name) $(step)"
	@$(compose) up -d

# Default install-app is on "dev" env, it installs react too
# If you need to do a prod target, define another one
install-app: clean-app build start

install: install-app